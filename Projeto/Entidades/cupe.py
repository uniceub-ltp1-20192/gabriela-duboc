from Entidades.automoveis import Automoveis


class Cupe(Automoveis):
  def __init__(self,numero_de_portas=2):
    super().__init__()
    self._numero_de_portas = numero_de_portas

  @property
  def numero_de_portas(self):
    return self._numero_de_portas

  @numero_de_portas.setter
  def numero_de_portas(self,numero_de_portas):
    self._numero_de_portas = numero_de_portas

  def correr(self):
    print("vrummmmmm")

  def __str__(self):
    return'''
  --- Cupe ---
  Identificador {}
  Cor: {}
  Numero de portas: {}
  Marca: {}
  Velocidade: {}
  Potencia: {}
  '''.format(self.identificador,self.cor,self._numero_de_portas,
             self.marca,self.velocidade,self.potencia)
